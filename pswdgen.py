import random

def generate_password(n=11, caractere="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789$"):
	s = random_char(n, caractere)
	print(s)

def random_char(n=11, caractere="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789$"):
	if n == 0:
		return ""
	return	random.choice(caractere) + random_char(n-1, caractere)

if __name__=='__main__':
    import sys
    try:
        func = sys.argv[1]
    except:
        func = None
    if func:
        exec(func)
    else:
        print("Erreur.")
    exit()
